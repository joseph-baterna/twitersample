package com.jb.yutweetmi.preference;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * Created by jb on 18/08/2016.
 */
public class MainPreference extends Application {

    private SharedPreferences sharedPreferences;
    private Editor editor;

    public MainPreference(Context context) {
        sharedPreferences = context.getSharedPreferences("twitter", context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    /**
     * Save twitter session
     * @param username
     * @param userId
     * @param token
     * @param secret
     */
    public void saveTwitterSession(String username, String userId, String token, String secret) {
        editor.putString("username", username);
        editor.putString("user_id", userId);
        editor.putString("token", token);
        editor.putString("secret", secret);
        editor.apply();
    }

    /**
     * Get twitter session
     * @return
     */
    public String getTwitterSession(String key) {
        return sharedPreferences.getString(key, "");
    }

}
