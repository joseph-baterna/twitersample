package com.jb.yutweetmi.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jb.yutweetmi.R;
import com.jb.yutweetmi.preference.MainPreference;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.models.User;
import com.twitter.sdk.android.tweetui.TimelineResult;
import com.twitter.sdk.android.tweetui.UserTimeline;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jb on 18/08/2016.
 */
public class HomeActivity extends AppCompatActivity {

    private static final String TAG = HomeActivity.class.getSimpleName();

    private MainPreference prefs;
    private Activity activity;
    private UserTimeline timeline;

    @BindView(R.id.img_profile_pic)
    ImageView imgProfilePic;

    @BindView(R.id.txt_username)
    TextView txtUsername;

    private Gson gson;

    public static void getStartIntent(Context context) {
        Intent intent = new Intent(context, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    @Override
    protected  void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        activity = this;
        prefs = new MainPreference(activity);
        this.gson = new Gson();

        initializeTwitterDetails();
    }

    private void initializeTwitterDetails() {
        timeline = new UserTimeline.Builder()
                .screenName(prefs.getTwitterSession("username"))
                .build();

        timeline.next(1L, new Callback<TimelineResult<Tweet>>() {
            @Override
            public void success(Result<TimelineResult<Tweet>> result) {
                for(Tweet tweet : result.data.items) {
                    Log.e(TAG, "Tweet => " + tweet.text);
                    Log.e(TAG, "User => " + tweet.user.screenName);
                    Log.e(TAG, "=================================================================");
                }
            }

            @Override
            public void failure(TwitterException exception) {

            }
        });
    }



}
