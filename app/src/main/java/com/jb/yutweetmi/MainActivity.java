package com.jb.yutweetmi;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.jb.yutweetmi.activities.HomeActivity;
import com.jb.yutweetmi.preference.MainPreference;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "OxEKCWBBYYi6PoJBFscXkjH6U";
    private static final String TWITTER_SECRET = "K2YndDoMceqLfQBUVacRJzh1dIgqDtPtftTZw6h8JFY32p99wK";

    private MainPreference prefs;
    private Activity activity;

    @BindView(R.id.btn_twitter_login) TwitterLoginButton btnTwitterLogin;
    @BindView(R.id.btn_login) Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        activity = this;
        prefs = new MainPreference(this);

        btnTwitterLogin.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                Log.e(TAG, "Secret => " + result.data.getAuthToken().secret);
                Log.e(TAG, "Token => " + result.data.getAuthToken().token);
                Log.e(TAG, "Username => " + result.data.getUserName());
                Log.e(TAG, "UserId => " + result.data.getUserId());
                Log.e(TAG, "Id => " + result.data.getId());

                // Save Twitter Session
                prefs.saveTwitterSession(
                        result.data.getUserName(),
                        String.valueOf(result.data.getUserId()),
                        result.data.getAuthToken().token,
                        result.data.getAuthToken().secret);

                HomeActivity.getStartIntent(activity);
                finish();
            }

            @Override
            public void failure(TwitterException exception) {
                Log.e(TAG, "Failed => " + exception.getLocalizedMessage());
                Toast.makeText(MainActivity.this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        btnLogin.setOnClickListener(this);
    }

    private void authenticateCient() {
        TwitterAuthClient authClient = new TwitterAuthClient();
        authClient.authorize(this, new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                Log.e(TAG, "Secret => " + result.data.getAuthToken().secret);
                Log.e(TAG, "Token => " + result.data.getAuthToken().token);
                Log.e(TAG, "Username => " + result.data.getUserName());
                Log.e(TAG, "UserId => " + result.data.getUserId());
                Log.e(TAG, "Id => " + result.data.getId());

                // Save Twitter Session
                prefs.saveTwitterSession(
                        result.data.getUserName(),
                        String.valueOf(result.data.getUserId()),
                        result.data.getAuthToken().token,
                        result.data.getAuthToken().secret);

                HomeActivity.getStartIntent(activity);
                finish();
            }

            @Override
            public void failure(TwitterException exception) {
                Log.e(TAG, "Failed => " + exception.getLocalizedMessage());
                Toast.makeText(MainActivity.this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result to the login button.
        btnTwitterLogin.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.btn_login: {
                authenticateCient();
            }
        }
    }
}
